
function addAndMultiply(x, y, z) {
  return ((x + y) * z)
}

console.log(addAndMultiply(4,8,5)) // 60

const addAndMultiply2 = (x, y, z) => ((x + y) * z);
console.log(addAndMultiply2(4,8,5)) // 60

// Also fix the string interpolation using Template Literals
function selfIntro(name, hobby, count) {
  return "My name is " + name + ". I practice " + hobby + " " + count + " times a day."
}

console.log(selfIntro('Shanqyeet', 'Kendama', '18')) // My name is Shanqyeet. I practice Kendama 18 times a day.

const selfIntro2 = (name, hobby, count) => `My name is ${name}. I practice ${hobby} ${count} times a day.`;
console.log(selfIntro2('Shanqyeet', 'Kendama', '18'));

// tips: remove curly braces
let squared = (x) => {
  return x * x
}

const squared2 = x => x * x;

console.log(squared(2));
console.log(squared2(2));

const prices = [1.00, 2.00, 3.00, 4.00]
const gst = 0.06


const pricesWithTax = prices.map(price => price * (1 + gst)); // Complete this with .map
const pricesWithTax2 = [];

prices.forEach(function(price){
  pricesWithTax2.push(price * (1 + gst));
});

console.log(pricesWithTax) // you should get [1.06, 2.12, 3.18, 4.24]
console.log(pricesWithTax2)