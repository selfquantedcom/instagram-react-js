// class Car {
//   constructor(owner) {
//     this.brand = 'Tesla'
//     this.model = 'Model X'
//     this.owner = owner
//   }

//   drive() {
//     console.log(`${this.owner.name} is driving his ${this.brand}`)
//   }

//   doSomethingTwice(action) {
//     for (let i = 0; i < 2; i++) {
//        action()
//     }
//   }
// }

// const mycar = new Car({
//   name: 'Nicholas',
//   age: 21,
//   gender: 'male'
// })

// mycar.doSomethingTwice(mycar.drive) // Cannot read property 'owner' of undefined

const getState = state => {
  // complete this
  const logState = () => console.log(`Your state is ${state}`)
  return [state, logState]
}

const [state, logState] = getState('stable')
console.log(state) // The console should print out 'stable'
logState() // The console should print out 'Your state is stable'
